<?php

namespace AppBundle\Command;


use AppBundle\Entity\Page;
use AppBundle\Entity\PageMeta;
use Box\Spout\Reader\ReaderInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

/**
 * Class DumpData
 * @package AppBundle\Command
 */
class SystemPageCreator extends ContainerAwareCommand
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @var KernelInterface
     */
    private $kernel;

    /**
     * DataUpload constructor.
     * @param null $name
     * @param KernelInterface $kernel
     * @param EntityManagerInterface $manager
     */
    public function __construct($name = null, KernelInterface $kernel, EntityManagerInterface $manager)
    {
        parent::__construct($name);
        $this->kernel = $kernel;
        $this->manager = $manager;
    }

    /**\
     * @see http://opensource.box.com/spout/docs/
     *
     *  Use:
     *  php bin/console data:page -p /data/system_page.xlsx -u true
     */
    protected function configure()
    {
        $this
            ->setName('data:page')
            ->setDescription('Create standard system page')
            ->addOption('path', 'p', InputOption::VALUE_REQUIRED, 'relative path')
            ->addOption('update', 'u', InputOption::VALUE_OPTIONAL, 'updating existing pages', false);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null
     * @throws \Box\Spout\Common\Exception\UnsupportedTypeException
     * @throws \Box\Spout\Reader\Exception\ReaderNotOpenedException
     * @throws \Doctrine\DBAL\ConnectionException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $path = sprintf('%s/Resources%s', $this->kernel->getRootDir(), $input->getOption('path'));
        $output->writeln(sprintf('<info>Upload data is begining from %s</info>', $path));
        $reader = ReaderFactory::create(Type::XLSX); // for XLSX files
        try {
            $reader->open($path);
        } catch (\Exception $e) {
            $output->writeln(sprintf('<error>%s%s</error>', $e->getMessage(), PHP_EOL));
        }
        $data = $this->getDataArray($reader);
        $this->manager->getConnection()->beginTransaction();
        foreach ($data as $item) {
            $page = $this->getPage($item, $input->getOption('update'));
            $this->manager->persist($page);
        }
        try {
            $this->manager->commit();
            $this->manager->flush();
            $output->writeln('<info>Data adding in DB</info>');
        } catch (\Exception $e) {
            $this->manager->getConnection()->rollBack();
            $reader->close();
            $output->writeln(sprintf('<error>%s%s</error>', $e->getMessage(), PHP_EOL));
        }

        return 0;
    }

    /**
     * @param ReaderInterface $reader
     * @return array
     * @throws \Box\Spout\Reader\Exception\ReaderNotOpenedException
     */
    protected function getDataArray(ReaderInterface $reader)
    {
        $data = [];
        foreach ($reader->getSheetIterator() as $key => $sheet) {
            $key--;
            $data[$key]['label'] = $sheet->getName();
            $data[$key]['data'] = [];
            $keys = null;
            foreach ($sheet->getRowIterator() as $row) {
                if (empty($keys)) {
                    $keys = $row;
                    continue;
                }
                if (!empty($row[2]) && !empty(trim($row[2]))) {
                    $data[$key]['data'][$row[2]] = [];
                }
                if (!empty($row[0])) {
                    $data[$key]['data'][$row[2]]['title'] = $row[0];
                }
                if (!empty($row[1]) && !empty(trim($row[1]))) {
                    $data[$key]['data'][$row[2]]['content'] = $row[1];
                }
            }
        }

        return $data;
    }

    /**
     * @param array $data
     * @param bool $update
     * @return PageMeta
     */
    private function getPage(array $data, $update = false)
    {
        $pageMeta = $this->searchMetaPage($data['label'], $update);
        foreach ($data['data'] as $key => $item) {
            $item['lang'] = $key;
            $page = $this->searchPage($item, $pageMeta->getId());
            $pageMeta->addPage($page);
        }

        return $pageMeta;
    }

    /**
     * @param string $label
     * @param boolean $update
     * @return PageMeta
     */
    private function searchMetaPage($label, $update)
    {
        $pageMeta = null;
        if ($update) {
            $pageMeta = $this->manager->getRepository('AppBundle:PageMeta')
                ->findOneBy(['label' => $label, 'system' => 1]);
        }
        if ($pageMeta) {
            return $pageMeta;
        }

        $pageMeta = new PageMeta();
        $pageMeta->setLabel($label);
        $pageMeta->setSystem(1);

        return $pageMeta;
    }

    /**
     * @param array $data
     * @param int $id
     * @return Page
     */
    private function searchPage(array $data, $id = null)
    {
        $page = null;
        if (!$id) {
            $page = new Page();
        } else {
            $page = $this->manager->getRepository('AppBundle:Page')
                ->findOneBy([
                    'meta' => $id,
                    'lang' => $data['lang']
                ]);
        }
        if (!$page) {
            $page = new Page();
        }
        if (isset($data['content'])) {
            $page->setContent($data['content']);
        }
        if (isset($data['title'])) {
            $page->setTitle($data['title']);
        }
        $page->setLang($data['lang']);

        return $page;
    }
}
<?php

namespace AppBundle\Command;


use Box\Spout\Reader\ReaderInterface;
use Doctrine\ORM\EntityManagerInterface;
use Jluct\AskBundle\Entity\Category;
use Jluct\AskBundle\Entity\CategoryData;
use Jluct\AskBundle\Entity\Question;
use Jluct\AskBundle\Entity\TypeQuestion;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

/**
 * Class DumpData
 * @package AppBundle\Command
 */
class DataUpload extends ContainerAwareCommand
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @var KernelInterface
     */
    private $kernel;

    /**
     * DataUpload constructor.
     * @param null $name
     * @param KernelInterface $kernel
     * @param EntityManagerInterface $manager
     */
    public function __construct($name = null, KernelInterface $kernel, EntityManagerInterface $manager)
    {
        parent::__construct($name);
        $this->kernel = $kernel;
        $this->manager = $manager;
    }

    /**\
     * @see http://opensource.box.com/spout/docs/
     *
     *  Use:
     *  php bin/console data:upload -p /data/category.ru.xlsx
     */
    protected function configure()
    {
        $this
            ->setName('data:upload')
            ->setDescription('Add data in DB from file. All files read only app/Resources/files/. Use relative path to file')
            ->addOption('path', 'p', InputOption::VALUE_REQUIRED, 'relative path');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     *
     * @see https://phpspreadsheet.readthedocs.io/en/develop/topics/reading-files/
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $path = sprintf('%s/Resources%s', $this->kernel->getRootDir(), $input->getOption('path'));
        $output->writeln(sprintf('<info>Upload data is begining from %s</info>', $path));
        $fileName = explode('.', $input->getOption('path'));
        $locale = $fileName[count($fileName) - 2];
        $reader = ReaderFactory::create(Type::XLSX); // for XLSX files
        $reader->open($path);
        $data = $this->getDataArray($reader);
        $this->manager->getConnection()->beginTransaction();
        foreach ($data as $item) {
            $item['locale'] = $locale;
            $this->manager->persist($this->createCategory($item));
        }
        try {
            $this->manager->commit();
            $this->manager->flush();
        } catch (\Exception $e) {
            $this->manager->getConnection()->rollBack();
            $output->writeln(sprintf('<error>%s%s</error>', $e->getMessage(), PHP_EOL));
        }
        $reader->close();
        $output->writeln('<info>Data adding in DB</info>');

        return 0;
    }

    /**
     * @param array $data
     * @return Category
     */
    protected function createCategory(array $data)
    {
        $category = new Category();
        $categoryData = new CategoryData();
        $categoryData->setName($data['category']);
        $categoryData->setCreatedAt(new \DateTime());
        $categoryData->setDescription(isset($data['description']) ? $data['description'] : '');
        $categoryData->setLang($data['locale']);
        $category->addData($categoryData);
        $category->setActive(true);
        $category->setImage($data['image']);
        foreach ($data['questions'] as $item) {
            $category->addQuestion($this->createQuestion($item));
        }

        return $category;
    }

    /**
     * @param $item
     * @return Question
     */
    protected function createQuestion($item)
    {
        $question = new Question();
        $question->setText($item);
        $question->setActive(true);
        $question->setType(
            $this->manager
                ->getRepository('AskBundle:TypeQuestion')
                ->findOneBy(['name' => TypeQuestion::TYPE_QUESTION['range']])
        );

        return $question;
    }

    /**
     * @param ReaderInterface $reader
     * @return array
     */
    protected function getDataArray(ReaderInterface $reader)
    {
        $data = [];
        foreach ($reader->getSheetIterator() as $key => $sheet) {
            $key--;
            $data[$key] = [];
            $keys = null;
            foreach ($sheet->getRowIterator() as $row) {
                if (empty($keys)) {
                    $keys = $row;
                    continue;
                }
                if (!empty($row[0])) {
                    $data[$key]['category'] = $row[0];
                }
                if (!empty($row[1]) && !empty(trim($row[1]))) {
                    $data[$key]['questions'][] = $row[1];
                }
                if (!empty($row[2]) && !empty(trim($row[2]))) {
                    $data[$key]['image'] = $row[2];
                }
            }
        }

        return $data;
    }
}
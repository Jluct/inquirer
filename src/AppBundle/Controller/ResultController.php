<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ResultController
 * @package AppBundle\Controller
 */
class ResultController extends Controller
{
    const PATH_SUMMARY = 'summary';

    /**
     * @param $period
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function summaryAction($period, Request $request)
    {
        $summary = $this->getDoctrine()
            ->getRepository('AskBundle:Category')
            ->summaryByPeriod(
                $request->getLocale(),
                $this->getParameter('date_separator'),
                $period
            );
        $value = array_column($summary, 'avg_value');
        $category = array_column($summary, 'name');

        return $this->render('AppBundle:ResultController:summary.html.twig', [
            'period' => $period,
            'value' => $value,
            'category' => $category
        ]);
    }
}

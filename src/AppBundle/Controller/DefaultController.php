<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Feedback;
use AppBundle\Entity\PageMeta;
use AppBundle\Form\FeedbackType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\CssSelector\Exception\InternalErrorException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class DefaultController
 * @package AppBundle\Controller
 */
class DefaultController extends Controller
{
    const PATH_INDEX = 'homepage';
    const PATH_HOW_USE = 'how-use';
    const PATH_SEARCH_CATEGORY = 'search-category';
    const PATH_ADDITIONAL_PAGE = 'additional_page';
    const PATH_FEEDBACK_REQUEST = 'feedback_request';

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        return $this->render('@App/default/index.html.twig', []);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function additionalPageAction(Request $request)
    {
        $locale = $request->getLocale();
        $label = $request->get('label');
        $rep = $this->getDoctrine()
            ->getManager()
            ->getRepository(PageMeta::class);
        $pageMeta = $rep->getPageBy($label, $locale);
        if ($pageMeta && $pageMeta->getPages()) {
            $page = $pageMeta->getPages()[0];
        } else {
            throw new NotFoundHttpException('Page is not defined');
        }

        return $this->render('@App/default/page-additional.html.twig', [
            'page' => $page,
            'label' => $pageMeta->getLabel()
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws InternalErrorException
     */
    public function howUseAction(Request $request)
    {
        $locale = $request->getLocale();
        $rep = $this->getDoctrine()
            ->getManager()
            ->getRepository(PageMeta::class);
        $pageMeta = $rep->getPageBy(PageMeta::PAGE_HOW_USE, $locale);
        if ($pageMeta && $pageMeta->getPages()) {
            $page = $pageMeta->getPages()[0];
        } else {
            throw new NotFoundHttpException(sprintf('Data \"%s\" is not defined', PageMeta::PAGE_HOW_USE));
        }

        return $this->render('@App/default/page-system.html.twig', [
            'page' => $page,
            'label' => $pageMeta->getLabel()
        ]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function changeLocaleAction()
    {
        return $this->redirectToRoute(self::PATH_INDEX);
    }

    /**
     * @see https://github.com/KnpLabs/KnpPaginatorBundle
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function searchCategoryAction(Request $request)
    {
        $searchData = $request->get('search');
        $categoryRepository = $this->getDoctrine()->getRepository('AskBundle:Category');
        $query = $categoryRepository->searchCategory($searchData);
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', $this->getParameter('app.pagination.page')),
            $this->getParameter('app.pagination.count')
        );

        return $this->render('@App/default/search.html.twig', [
            'searchData' => $searchData,
            'pagination' => $pagination
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function FeedbackAction(Request $request)
    {
        $feedback = new Feedback();
        $form = $this->createForm(FeedbackType::class, $feedback);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $feedback->setCreatedAt(new \DateTime());
            $feedback->setUser($this->getUser());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($feedback);
            try {
                $entityManager->flush();
                $this->addFlash(
                    'success',
                    'Ваше сообщение принято'
                );
            } catch (\Exception $e) {
                $this->addFlash(
                    'danger',
                    'Ошибка отправки формы'
                );
            }
        }

        return $this->redirect($request->server->get('HTTP_REFERER'));
    }
}

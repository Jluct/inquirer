<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Page;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Class PageFixtures
 * @package AppBundle\DataFixtures\ORM
 */
class PageFixtures extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface, ORMFixtureInterface
{
    /**
     * @var array
     */
    private $locale = ['en', 'ru'];

    /**
     * @param ObjectManager $manager
     * TODO: Сократить размер заголовка
     */
    public function load(ObjectManager $manager)
    {
        $count = count($this->getEnData());

        for ($i = 0; $i < $count; $i++) {
            $pageMeta = $this->getReference('pageMeta' . ($i + 1));

            foreach ($this->locale as $item) {
                $page = $this->generatePageData($i, $item);
                $page->setMeta($pageMeta);
                $manager->persist($page);
            }

            try {
                $manager->flush();
            } catch (\Exception $e) {
                echo $e->getMessage();
            }
        }
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 7;
    }

    /**
     * @param $locale
     * @return mixed
     */
    private function getLangData($locale)
    {
        return call_user_func([$this, sprintf('get%sData', ucfirst($locale))]);
    }

    /**
     * @param $i
     * @param string $locale
     * @return Page
     */
    private function generatePageData($i, $locale)
    {
        $page = new Page();
        $page->setTitle($this->getLangData($locale)[$i]);
        $page->setContent($this->getDescription($locale));
        $page->setLang($locale);

        $page->setCreatedAt(new \DateTime());

        return $page;
    }

    /**
     * @return array
     */
    private function getEnData()
    {
        return [
            'Lorem ipsum dolor sit amet consectetur adipiscing elit',
            'Pellentesque vitae velit ex',
            'Mauris dapibus risus quis suscipit vulputate',
            'Eros diam egestas libero eu vulputate risus',
            'In hac habitasse platea dictumst',
            'Morbi tempus commodo mattis',
            'Ut suscipit posuere justo at vulputate',
            'Ut eleifend mauris et risus ultrices egestas',
            'Aliquam sodales odio id eleifend tristique',
            'Urna nisl sollicitudin id varius orci quam id turpis',
            'Nulla porta lobortis ligula vel egestas',
            'Curabitur aliquam euismod dolor non ornare',
            'Sed varius a risus eget aliquam',
            'Nunc viverra elit ac laoreet suscipit',
            'Pellentesque et sapien pulvinar consectetur',
            'Ubi est barbatus nix',
            'Abnobas sunt hilotaes de placidus vita',
            'Ubi est audax amicitia',
            'Eposs sunt solems de superbus fortis',
            'Vae humani generis',
            'Diatrias tolerare tanquam noster caesium',
            'Teres talis saepe tractare de camerarius flavum sensorem',
            'Silva de secundus galatae demitto quadra',
            'Sunt accentores vitare salvus flavum parses',
            'Potus sensim ad ferox abnoba',
            'Sunt seculaes transferre talis camerarius fluctuies',
            'Era brevis ratione est',
            'Sunt torquises imitari velox mirabilis medicinaes',
            'Mineralis persuadere omnes finises desiderium',
            'Bassus fatalis classiss virtualiter transferre de flavum'
        ];
    }

    /**
     * @return array
     */
    private function getRuData()
    {
        return [
            'Значимость этих проблем настолько очевидна, что сложившаяся структура организации обеспечивает широкому кругу (специалистов) участие в формировании новых предложений.',
            'Идейные соображения высшего порядка, а также реализация намеченных плановых заданий требуют от нас анализа систем массового участия.',
            'Разнообразный и богатый опыт начало повседневной работы по формированию позиции способствует подготовки и реализации систем массового участия.',
            'Задача организации, в особенности же консультация с широким активом позволяет оценить значение модели развития.',
            'Равным образом укрепление и развитие структуры требуют определения и уточнения направлений прогрессивного развития.',
            'Идейные соображения высшего порядка, а также реализация намеченных плановых заданий способствует подготовки и реализации направлений прогрессивного развития.',
            'Равным образом постоянный количественный рост и сфера нашей активности играет важную роль в формировании систем массового участия.',
            'Разнообразный и богатый опыт сложившаяся структура организации влечет за собой процесс внедрения и модернизации систем массового участия.',
            'Не следует, однако забывать, что новая модель организационной деятельности требуют определения и уточнения новых предложений',
            'Задача организации, в особенности же дальнейшее развитие различных форм деятельности влечет за собой процесс внедрения и модернизации системы обучения кадров, соответствует насущным потребностям.',
            'С другой стороны дальнейшее развитие различных форм деятельности позволяет оценить значение существенных финансовых и административных условий',
            'Задача организации, в особенности же сложившаяся структура организации играет важную роль в формировании форм развития.',
            'С другой стороны сложившаяся структура организации влечет за собой процесс внедрения и модернизации модели развития',
            'Задача организации, в особенности же постоянный количественный рост и сфера нашей активности позволяет оценить значение новых предложений',
            'Не следует, однако забывать, что сложившаяся структура организации требуют определения и уточнения систем массового участия',
            'Равным образом рамки и место обучения кадров требуют от нас анализа форм развития.',
            'Повседневная практика показывает, что консультация с широким активом способствует подготовки и реализации соответствующий условий активизации',
            'Постоянное информационно-пропагандистское обеспечение нашей деятельности позволяет выполнять важные задания по разработке дальнейших направлений развития',
            'Разнообразный и богатый опыт сложившаяся структура организации представляет собой интересный эксперимент проверки системы обучения кадров, соответствует насущным потребностям.',
            'Таким образом укрепление и развитие структуры в значительной степени обуславливает создание модели развития.',
            'Задача организации, в особенности же сложившаяся структура организации в значительной степени обуславливает создание соответствующий условий активизации.',
            'Равным образом новая модель организационной деятельности представляет собой интересный эксперимент проверки позиций, занимаемых участниками в отношении поставленных задач.',
            'Равным образом рамки и место обучения кадров влечет за собой процесс внедрения и модернизации соответствующий условий активизации.',
            'Значимость этих проблем настолько очевидна, что консультация с широким активом способствует подготовки и реализации направлений прогрессивного развития.',
            'Повседневная практика показывает, что укрепление и развитие структуры позволяет выполнять важные задания по разработке систем массового участия.',
            'Разнообразный и богатый опыт постоянный количественный рост и сфера нашей активности влечет за собой процесс внедрения и модернизации позиций, занимаемых участниками в отношении поставленных задач.',
            'С другой стороны реализация намеченных плановых заданий позволяет оценить значение направлений прогрессивного развития.',
            'Значимость этих проблем настолько очевидна, что реализация намеченных плановых заданий играет важную роль в формировании соответствующий условий активизации.',
            'Задача организации, в особенности же начало повседневной работы по формированию позиции позволяет оценить значение систем массового участия.',
            'Равным образом рамки и место обучения кадров требуют от нас анализа форм развития.'
        ];
    }

    /**
     * @param string $locale
     * @return string
     */
    private function getDescription($locale)
    {
        $data = $this->getLangData($locale);
        $start_num = mt_rand(0, count($data) - 3);
        $end_num = mt_rand($start_num, count($data) - 1);
        shuffle($data);

        return mb_strimwidth(implode(' ', array_slice($data, $start_num, $end_num)), 0, 500);
    }
}
<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\PageMeta;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class PageMetaFixtures
 * @package AppBundle\DataFixtures\ORM
 */
class PageMetaFixtures extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface, ORMFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $count = count($this->getLabel());

        for ($i = 0; $i < $count; $i++) {
            $pageMeta = $this->generatePageMeta($i);
            $manager->persist($pageMeta);
            $manager->flush();

            $this->addReference('pageMeta' . ($i + 1), $pageMeta);
        }
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 6;
    }

    /**
     * @param int $i
     * @return PageMeta
     */
    private function generatePageMeta($i)
    {
        $pageMeta = new PageMeta();
        $pageMeta->setLabel($this->getLabel()[$i]);
        $pageMeta->setSystem(0);

        return $pageMeta;
    }

    /**
     * @return array
     */
    private function getLabel()
    {
        return [
            'homepage',
            'how-use',
            'lorem',
            'ipsum',
            'test',
            'page6',
            'page7',
            'page8',
            'page9',
            'page10',
            'page11',
            'page12',
            'page13',
            'page14',
            'page15',
            'page16',
            'page17',
            'page18',
            'page19',
            'page21',
            'page22',
            'page23',
            'page24',
            'page25',
            'page26',
            'page27',
            'page28',
            'page29',
            'page30',
            'page31',
        ];
    }
}
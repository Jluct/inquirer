<?php

namespace AppBundle\Twig\Extension;

use Doctrine\ORM\EntityManagerInterface;

/**
 * Class AdditionalPageExtension
 * @package AppBundle\Twig\Extension
 */
class AdditionalPageExtension extends \Twig_Extension
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * AdditionalPageExtension constructor.
     * @param EntityManagerInterface $manager
     */
    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @inheritdoc
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('additional_page', [$this, 'getAdditionalPages'])
        ];
    }

    /**
     * @param string $locale
     * @return array|null
     */
    public function getAdditionalPages($locale)
    {
        return $this->manager->getRepository('AppBundle:PageMeta')->getAdditionalPage($locale);
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'additional_page_extension';
    }
}
<?php

namespace AppBundle\Twig\Extension;

use AppBundle\Entity\Feedback;
use AppBundle\Form\FeedbackType;
use Symfony\Component\Form\FormFactoryInterface;

/**
 * Class FeedbackFormExtension
 * @package AppBundle\Twig\Extension
 */
class FeedbackFormExtension extends \Twig_Extension
{
    /**
     * @var FormFactoryInterface
     */
    private $factoryBuilder;

    /**
     * FeedbackFormExtension constructor.
     * @param FormFactoryInterface $factoryBuilder
     */
    public function __construct(FormFactoryInterface $factoryBuilder)
    {
        $this->factoryBuilder = $factoryBuilder;
    }

    /**
     * @inheritdoc
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('feedbackform', [$this, 'getForm'])
        ];
    }

    /**
     * @return \Symfony\Component\Form\FormView
     */
    public function getForm()
    {
        $feedback = new Feedback();
        $form = $this->factoryBuilder->createBuilder(FeedbackType::class, $feedback);

        return $form->getForm()->createView();
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'feedbackform_extension';
    }
}
<?php

namespace AppBundle\Twig\Extension;

use Symfony\Component\Routing\RouterInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * Class BreadcrumbExtension
 * @package AppBundle\Twig\Extension
 */
class BreadcrumbExtension extends \Twig_Extension
{
    /**
     * @var Breadcrumbs
     */
    private $breadcrumbs;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var string
     */
    private $homeRoute;

    /**
     * @var string
     */
    private $homeLabel;

    /**
     * @param Breadcrumbs $breadcrumbs
     * @param RouterInterface $router
     * @param string $homeRoute
     * @param string $homeLabel
     */
    public function __construct(Breadcrumbs $breadcrumbs, RouterInterface $router, $homeRoute = 'homepage', $homeLabel = 'homepage')
    {
        $this->breadcrumbs = $breadcrumbs;
        $this->router = $router;
        $this->homeRoute = $homeRoute;
        $this->homeLabel = $homeLabel;
    }

    /**
     * @inheritdoc
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('breadcrumb', [$this, 'addBreadcrumb'])
        ];
    }

    /**
     * @param $label
     * @param string $url
     * @param array $translationParameters
     */
    public function addBreadcrumb($label, $url = '', array $translationParameters = [])
    {
        if (!$this->breadcrumbs->count()) {
            $this->breadcrumbs->addItem($this->homeLabel, $this->router->generate($this->homeRoute),[]);
        }
        $this->breadcrumbs->addItem($label, $url, $translationParameters);
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'breadcrumb_extension';
    }
}
<?php

namespace Jluct\AskBundle\Repository;

/**
 * AnswerRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class AnswerRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param array $data
     * @return null|object
     */
    public function findExistAnswer(array $data)
    {
        return $this->findOneBy(
            [
                'question' => $data['question'],
                'user' => $data['user']
            ],
            [
                'createdAt' => 'DESC'
            ]
        );
    }
}

<?php

namespace Jluct\AskBundle\Service;

use Jluct\AskBundle\Entity\Category;

/**
 * Class Tree
 * @package Jluct\AskBundle\Service
 */
class Tree
{
    /**
     * @var array
     */
    private $chain = [];

    /**
     * @param array $array
     * @return bool
     */
    public function isChain(array $array)
    {
        for ($i = 0; $i < count($array); $i++) {
            if ($i == 0 && !$array[$i]->getParent()) {
                continue;
            } elseif ($i == 0 && $array[$i]->getParent()) {
                return false;
            }
            if ($array[$i]->getParent()->getId() !== $array[$i - 1]->getId()) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param array $array
     * @return string
     */
    public function getChainUrl(array $array)
    {
        $url = '';
        foreach ($array as $item) {
            $url .= sprintf('%s/', $item->getId());
        }

        return $url;
    }

    /**
     * @param Category $category
     * @return array
     */
    public function loadingChain(Category $category)
    {
        if (empty($this->chain)) {
            $this->chain[] = $category;
        }
        $parent = $category->getParent();
        if ($parent) {
            $this->chain[] = $parent;
            $this->loadingChain($parent);
        }

        return $this->chain;
    }
}
<?php

namespace Jluct\AskBundle\Service;


use Doctrine\ORM\EntityManagerInterface;
use Jluct\AskBundle\Entity\Question;
use Jluct\AskBundle\Entity\QuestionRule;
use Jluct\AskBundle\Entity\TypeQuestion;

/**
 * Class ValidateQuestionService
 * @package Jluct\AskBundle\Service
 */
class ValidateQuestionService
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @var Question
     */
    private $question;

    /**
     * ValidateQuestionService constructor.
     * @param EntityManagerInterface $manager
     */
    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @return TypeQuestion
     */
    public function getType()
    {
        return $this->question->getType();
    }

    /**
     * @return array
     */
    public function getRules()
    {
        /** @var TypeQuestion $type */
        $type = $this->question->getType();

        $rules = $type->getRules();
        $rulesArray = [];
        /** @var QuestionRule $item */
        foreach ($rules as $item) {
            $rulesArray[$item->getCond()] = $item->getValue();
        }

        return $rulesArray;
    }

    /**
     * @return mixed
     */
    public function getAssert()
    {
        $type = 'Symfony\Component\Validator\Constraints\\' . $this->question->getType()->getType();

        return new $type($this->getRules());
    }

    /**
     * @param Question $question
     */
    public function setQuestion(Question $question)
    {
        $this->question = $question;
    }


}
<?php

namespace Jluct\AskBundle\Form;


use Jluct\AskBundle\Entity\Answer;
use Jluct\AskBundle\Entity\TypeQuestion;
use Jluct\AskBundle\Service\ValidateQuestionService;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\CallbackTransformer;

/**
 * Class AnswerType
 * @package Jluct\AskBundle\Form
 */
class AnswerType extends AbstractType
{
    /**
     * @var ValidateQuestionService
     */
    private $questionService;

    /**
     * AnswerType constructor.
     * @param ValidateQuestionService $questionService
     */
    public function __construct(ValidateQuestionService $questionService)
    {
        $this->questionService = $questionService;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $questionService = $this->questionService;
        $assert = $this->questionService->getAssert();
        $builder
            ->add('value',
                null,
                ['constraints' => [$assert]]
            )
            ->add('question', EntityType::class, [
                'class' => 'Jluct\AskBundle\Entity\Question',
            ]);
        $builder->get('value')
            ->addModelTransformer(new CallbackTransformer(
                function ($data) {
                    return $data;
                },
                function ($data) use ($questionService) {
                    if ($questionService->getType()->getName() == TypeQuestion::TYPE_QUESTION_NAME['bool']) {
                        $data = (bool)$data;
                    }

                    return $data;
                }
            ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Answer::class,
            'csrf_protection' => false
        ]);
    }
}
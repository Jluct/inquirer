<?php

namespace Jluct\AskBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class DefaultControllerTest
 * @package Jluct\AskBundle\Tests\Controller
 *
 * Run
 * php phpunit.phar src\Jluct\AskBundle\Tests\Controller\DefaultControllerTest
 */
class DefaultControllerTest extends WebTestCase
{
    /**
     * @var array
     */
    private $locale = ['en', 'ru'];

    /**
     * @var array
     */
    private $true_url = [
        [200, '/ask/2/8/14/20/26/'],
        [200, '/ask/4/10/16/22/28/'],
        [200, '/ask/5/11/17/23/29/'],
        [200, '/ask/6/12/18/']
    ];

    /**
     * @var array
     */
    private $false_url = [
        [404, '/ask/1/20/13/5/'],
        [404, '/ask/6/10/'],
        [404, '/ask/7/6/20/'],
        [404, '/ask/30/31'],
        [404, '/ask/12/18'],
        [404, '/ask/6/18'],
    ];

    /**
     * @var array
     */
    private $question = [
        ['position' => 0, 'id' => 1, 'class' => 'question-default'],
        ['position' => 1, 'id' => 2, 'class' => 'question-default'],
        ['position' => 4, 'id' => 5, 'class' => 'question-default'],
        ['position' => 5, 'id' => 6, 'class' => 'question-default'],

    ];

    /**
     * @var array
     */
    private $form_data = [
        ['question' => 0, 'value' => 'Test answer 0'],
        ['question' => 1, 'value' => 'Test answer 1'],
        ['question' => 2, 'value' => 'Test answer 2'],
        ['question' => 3, 'value' => null]
    ];

    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', sprintf('/%s/%s', $this->locale[0], 'ask'));

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Опрос населения', $crawler->filter('h1')->text());
        $this->assertEquals(6, $crawler->filter('.col-sm-6>h3>a')->count());

        $this->assertContains(
            sprintf('/%s/%s', $this->locale[0], 'ask/1/'),
            $crawler->filter('.col-sm-6 a')->eq(0)->attr('href')
        );
        $this->assertContains(
            sprintf('/%s/%s', $this->locale[0], 'ask/5/'),
            $crawler->filter('.col-sm-6 a')->eq(4)->attr('href')
        );
    }

    public function testUrl()
    {
        $client = static::createClient();
        foreach ($this->getAllUrl() as $item) {
            $client->request(
                'GET',
                sprintf('/%s%s', $this->locale[0], $item[1])
            );
            $this->assertEquals(
                $item[0],
                $client->getResponse()->getStatusCode(), "Error in assert, where URL is $item[1]"
            );
        }
    }

    public function testQuestion()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', sprintf('/%s%s', $this->locale[0], $this->true_url[0][1]));
        $this->assertEquals($this->true_url[0][0], $client->getResponse()->getStatusCode());
        $this->assertEquals(6, $crawler->filter('.col-sm-12>ol.breadcrumb>li')->count());

        foreach ($this->question as $item) {
            $this->assertEquals(
                $item["id"],
                $crawler->filter('.col-sm-6>div.form_container>form input[type=hidden]')
                    ->eq($item["position"])
                    ->attr('value'),
                "Hidden input with id question $item[id] not found");

            $this->assertEquals(
                'row ' . $item['class'],
                $crawler->filter('.col-sm-6>div.form_container>form>div')
                    ->eq($item["position"])->attr('class'),
                "Class $item[class] not found");
        }

    }

    public function testAnswer()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', $this->true_url[0][1]);
        $this->assertEquals($this->true_url[0][0], $client->getResponse()->getStatusCode());

        foreach ($this->form_data as $item) {
            $this->assertEquals(
                $this->question[$item['question']]['id'],
                $crawler->filter('.col-sm-6>div.form_container>form input[type=hidden]')
                    ->eq($this->question[$item['question']]["position"])
                    ->attr('value')
            );

            $form = $crawler
                ->filter('.col-sm-6>div.form_container>form input[type=submit]')
                ->eq($this->question[$item['question']]['position'])
                ->selectButton('submit')
                ->form([
                    "answer[value]" => $item['value'],
                    "answer[question_id]" => $this->question[$item['question']]['id']
                ]);
            $client->submit($form);
        }
    }

    /**
     * @return \Generator
     */
    private function getAllUrl()
    {
        yield from $this->true_url;
        yield from $this->false_url;
    }
}

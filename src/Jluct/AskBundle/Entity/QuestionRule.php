<?php

namespace Jluct\AskBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * QuestionRule
 *
 * @ORM\Table(name="question_rule")
 * @ORM\Entity(repositoryClass="Jluct\AskBundle\Repository\QuestionRuleRepository")
 */
class QuestionRule
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="cond", type="string", length=255)
     */
    private $cond;
    
    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=255)
     */
    private $value;
    
    /**
     * @var Question $type
     *
     * @ORM\ManyToOne(targetEntity="TypeQuestion", inversedBy="rules")
     * @ORM\JoinColumn(name="type_question_id", referencedColumnName="id")
     */
    private $type;
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set cond
     *
     * @param string $cond
     *
     * @return QuestionRule
     */
    public function setCond($cond)
    {
        $this->cond = $cond;
        
        return $this;
    }
    
    /**
     * Get cond
     *
     * @return string
     */
    public function getCond()
    {
        return $this->cond;
    }
    
    /**
     * Set value
     *
     * @param string $value
     *
     * @return QuestionRule
     */
    public function setValue($value)
    {
        $this->value = $value;
        
        return $this;
    }
    
    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }
    
    /**
     * Set question
     *
     * @param \Jluct\AskBundle\Entity\TypeQuestion $type
     *
     * @return QuestionRule
     */
    public function setType(\Jluct\AskBundle\Entity\TypeQuestion $type = null)
    {
        $this->type = $type;
        
        return $this;
    }
    
    /**
     * Get question
     *
     * @return \Jluct\AskBundle\Entity\Question
     */
    public function getType()
    {
        return $this->type;
    }
}

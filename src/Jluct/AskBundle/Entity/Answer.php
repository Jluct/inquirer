<?php

namespace Jluct\AskBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Jluct\UserBundle\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Answer
 *
 * @ORM\Table(name="answer")
 * @ORM\Entity(repositoryClass="Jluct\AskBundle\Repository\AnswerRepository")
 * @UniqueEntity(fields={"question", "user", "createdAt"})
 */
class Answer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $value
     *
     * @Assert\NotNull(
     *     message="Данное поле не может быть пустым"
     * )
     * @ORM\Column(name="value", type="string", length=255)
     *
     */
    private $value;

    /**
     * @var Question $question
     *
     * @ORM\ManyToOne(targetEntity="Question", inversedBy="answers")
     * @ORM\JoinColumn(name="question_id", referencedColumnName="id")
     *
     * @Assert\NotNull()
     * @Assert\Valid
     */
    private $question;

    /**
     * @var User $user
     *
     * @Assert\Valid
     * @Assert\NotBlank()
     *
     * @ORM\ManyToOne(targetEntity="Jluct\UserBundle\Entity\User", inversedBy="answers")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * Answer constructor.
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return Answer
     */
    public function setValue($value)
    {
        $this->value = $value === 0 || $value === false ? '0' : $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set question
     *
     * @param \Jluct\AskBundle\Entity\Question $question
     *
     * @return Answer
     */
    public function setQuestion(\Jluct\AskBundle\Entity\Question $question = null)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return \Jluct\AskBundle\Entity\Question
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set user
     *
     * @param \Jluct\UserBundle\Entity\User $user
     *
     * @return Answer
     */
    public function setUser(\Jluct\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Jluct\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Answer
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}

<?php

namespace Jluct\AskBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Category
 *
 * @ORM\Table(name="category")
 * @ORM\Entity(repositoryClass="Jluct\AskBundle\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     */
    private $active;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Category", mappedBy="parent")
     *
     */
    private $children;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="children", cascade={"persist"})
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Question", mappedBy="category", cascade={"persist"})
     */
    private $questions;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Jluct\AskBundle\Entity\CategoryData", mappedBy="category", cascade={"persist"})
     */
    private $data;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * Category constructor.
     */
    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->questions = new ArrayCollection();
        $this->data = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Category
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set parent
     *
     * @param Category $parent
     *
     * @return Category
     */
    public function setParent($parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return int
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add child
     *
     * @param \Jluct\AskBundle\Entity\Category $child
     *
     * @return Category
     */
    public function addChild(\Jluct\AskBundle\Entity\Category $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \Jluct\AskBundle\Entity\Category $child
     */
    public function removeChild(\Jluct\AskBundle\Entity\Category $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Add question
     *
     * @param \Jluct\AskBundle\Entity\Question $question
     *
     * @return Category
     */
    public function addQuestion(\Jluct\AskBundle\Entity\Question $question)
    {
        $this->questions[] = $question;
        $question->setCategory($this);

        return $this;
    }

    /**
     * Remove question
     *
     * @param \Jluct\AskBundle\Entity\Question $question
     */
    public function removeQuestion(\Jluct\AskBundle\Entity\Question $question)
    {
        $this->questions->removeElement($question);
    }

    /**
     * Get questions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * Add data
     *
     * @param \Jluct\AskBundle\Entity\CategoryData $data
     *
     * @return Category
     */
    public function addData(\Jluct\AskBundle\Entity\CategoryData $data)
    {
        $this->data[] = $data;
        $data->setCategory($this);
        return $this;
    }

    /**
     * Remove data
     *
     * @param \Jluct\AskBundle\Entity\CategoryData $data
     */
    public function removeData(\Jluct\AskBundle\Entity\CategoryData $data)
    {
        $this->data->removeElement($data);
    }

    /**
     * Get data
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Add datum
     *
     * @param \Jluct\AskBundle\Entity\CategoryData $datum
     *
     * @return Category
     */
    public function addDatum(\Jluct\AskBundle\Entity\CategoryData $datum)
    {
        $this->data[] = $datum;

        return $this;
    }

    /**
     * Remove datum
     *
     * @param \Jluct\AskBundle\Entity\CategoryData $datum
     */
    public function removeDatum(\Jluct\AskBundle\Entity\CategoryData $datum)
    {
        $this->data->removeElement($datum);
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage(string $image)
    {
        $this->image = $image;
    }
}

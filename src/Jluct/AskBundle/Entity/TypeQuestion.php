<?php

namespace Jluct\AskBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Jluct\AskBundle\Entity\Question;

/**
 * TypeQuestion
 *
 * @ORM\Table(name="type_question")
 * @ORM\Entity(repositoryClass="Jluct\AskBundle\Repository\TypeQuestionRepository")
 */
class TypeQuestion
{
    const TYPE_QUESTION = [
        'range' => 'Range',
        'bool' => 'Type',
        'string' => 'Length',
    ];

    const TYPE_QUESTION_NAME = [
        'bool' => 'bool'
    ];

    const TYPE_QUESTION_DEFAULT = self::TYPE_QUESTION['string'];

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @var Question $questions
     *
     * @ORM\OneToMany(targetEntity="Question", mappedBy="type")
     */
    private $questions;

    /**
     * @var ArrayCollection $rules
     *
     * @ORM\OneToMany(targetEntity="QuestionRule", mappedBy="type")
     */
    private $rules;

    /**
     * @var string
     * @ORM\Column(name="type", type="string", length=255, nullable=false)
     */
    private $type;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return TypeQuestion
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set questions
     *
     * @param Question $questions
     *
     * @return TypeQuestion
     */
    public function setQuestions(Question $questions = null)
    {
        $this->questions = $questions;

        return $this;
    }

    /**
     * Get questions
     *
     * @return Question
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->questions = new ArrayCollection();
        $this->rules = new ArrayCollection();
    }

    /**
     * Add question
     *
     * @param Question $question
     *
     * @return TypeQuestion
     */
    public function addQuestion(Question $question)
    {
        $this->questions[] = $question;

        return $this;
    }

    /**
     * Remove question
     *
     * @param Question $question
     */
    public function removeQuestion(Question $question)
    {
        $this->questions->removeElement($question);
    }

    /**
     * Add rule
     *
     * @param \Jluct\AskBundle\Entity\QuestionRule $rule
     *
     * @return TypeQuestion
     */
    public function addRule(\Jluct\AskBundle\Entity\QuestionRule $rule)
    {
        $this->rules[] = $rule;

        return $this;
    }

    /**
     * Remove rule
     *
     * @param \Jluct\AskBundle\Entity\QuestionRule $rule
     */
    public function removeRule(\Jluct\AskBundle\Entity\QuestionRule $rule)
    {
        $this->rules->removeElement($rule);
    }

    /**
     * Get rules
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRules()
    {
        return $this->rules;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }
}

<?php

namespace Jluct\AskBundle\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Jluct\AskBundle\Entity\TypeQuestion;

/**
 * Class TypeQuestionFixtures
 * @package Jluct\AskBundle\DataFixtures\ORM
 */
class TypeQuestionFixtures extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface, ORMFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        foreach ($this->type as $item) {
            $type = new TypeQuestion();
            $type->setName($item[0]);
            $type->setType($item[1]);
            $manager->persist($type);
            $manager->flush();
            $this->addReference('TypeQuestion' . $item[2], $type);
        }
    }

    /**
     * @var array
     */
    private $type = [
        ['default', 'Length', 1],
        ['range', 'Range', 2],
        ['bool', 'Type', 3],
        ['string', 'Length', 4]
    ];

    /**
     * @return int
     */
    public function getOrder()
    {
        return 1;
    }
}
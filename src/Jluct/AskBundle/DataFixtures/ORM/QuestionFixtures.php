<?php

namespace Jluct\AskBundle\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Jluct\AskBundle\Entity\Category;
use Jluct\AskBundle\Entity\Question;
use Jluct\AskBundle\Entity\TypeQuestion;

/**
 * Class QuestionFixtures
 * @package Jluct\AskBundle\DataFixtures\ORM
 */
class QuestionFixtures extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface, ORMFixtureInterface
{
    /**
     * @var array
     */
    private $category = [26, 27, 28, 29, 30];

    /**
     * @var Category $cat
     * @var Question $q
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $count = count($this->getPhrases());

        for ($i = 0; $i < $count; $i++) {

            $pr_i = (($i != 0 ? $i : 1) * 100) / ($count - 1);
            $r = round(((count($this->category) - 1) * $pr_i) / 100);
            $rt = ceil((4 * $pr_i) / 100);

            $cat = $this->getReference('category' . $this->category[(int)$r]);

            $q = $this->generateQuestion($i);
            $q->setType($manager->find(TypeQuestion::class, $rt));
            $cat->addQuestion($q);
            $q->setCategory($cat);

            $manager->persist($cat);
            $manager->flush();
            $this->addReference('question' . (string)$i, $q);
        }
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 4;
    }

    /**
     * @param $i
     * @return Question
     */
    private function generateQuestion($i)
    {
        $question = new Question();
        $question->setText($this->getPhrases()[$i]);
        $question->setActive((int)$i % 10);

        return $question;
    }

    /**
     * @return array
     */
    private function getPhrases()
    {
        return [
            'Lorem ipsum dolor sit amet consectetur adipiscing elit',
            'Pellentesque vitae velit ex',
            'Mauris dapibus risus quis suscipit vulputate',
            'Eros diam egestas libero eu vulputate risus',
            'In hac habitasse platea dictumst',
            'Morbi tempus commodo mattis',
            'Ut suscipit posuere justo at vulputate',
            'Ut eleifend mauris et risus ultrices egestas',
            'Aliquam sodales odio id eleifend tristique',
            'Urna nisl sollicitudin id varius orci quam id turpis',
            'Nulla porta lobortis ligula vel egestas',
            'Curabitur aliquam euismod dolor non ornare',
            'Sed varius a risus eget aliquam',
            'Nunc viverra elit ac laoreet suscipit',
            'Pellentesque et sapien pulvinar consectetur',
            'Ubi est barbatus nix',
            'Abnobas sunt hilotaes de placidus vita',
            'Ubi est audax amicitia',
            'Eposs sunt solems de superbus fortis',
            'Vae humani generis',
            'Diatrias tolerare tanquam noster caesium',
            'Teres talis saepe tractare de camerarius flavum sensorem',
            'Silva de secundus galatae demitto quadra',
            'Sunt accentores vitare salvus flavum parses',
            'Potus sensim ad ferox abnoba',
            'Sunt seculaes transferre talis camerarius fluctuies',
            'Era brevis ratione est',
            'Sunt torquises imitari velox mirabilis medicinaes',
            'Mineralis persuadere omnes finises desiderium',
            'Bassus fatalis classiss virtualiter transferre de flavum',
            'Nunc iaculis mauris ut dictum finibus',
            'Nullam porta nunc vitae urna finibus, dictum placerat odio volutpat',
            'Maecenas vulputate ante ut purus malesuada, in tincidunt justo dapibus',
            'Sed ac arcu a dui feugiat finibus',
            'Duis vehicula felis consectetur, tempus leo at, dignissim tellus',
            'Aliquam et leo egestas, volutpat libero sed, dictum orci',
            'Fusce sit amet lorem quis turpis lobortis facilisis',
            'Nunc volutpat enim ut arcu ultricies, quis euismod augue elementum',
            'Pellentesque at velit lobortis nisl imperdiet volutpat a sit amet ante',
            'Praesent egestas erat in tempor laoreet',
            'Aliquam at erat eget lorem gravida lacinia',
            'Nulla euismod felis sollicitudin aliquam aliquet',
            'Maecenas eget nibh vitae leo pretium gravida vitae eget metus',
            'Nullam facilisis ligula id laoreet egestas',
            'Donec a justo porta, rutrum arcu a, ultrices nibh',
            'Nullam sit amet felis hendrerit ante mollis elementum',
            'Nam facilisis risus commodo tristique iaculis',
            'Cras maximus elit in arcu mollis, quis rutrum mauris interdum',
        ];
    }
}

<?php

namespace Jluct\AskBundle\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Jluct\AskBundle\Entity\QuestionRule;

/**
 * Class QuestionRuleFixtures
 * @package Jluct\AskBundle\DataFixtures\ORM
 */
class QuestionRuleFixtures extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface, ORMFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        foreach ($this->getRules() as $item) {
            $type = $this->getReference('TypeQuestion' . $item[0]);
            $rule = new QuestionRule();
            $rule->setCond($item[1]);
            $rule->setValue($item[2]);

            $rule->setType($type);

            $manager->persist($rule);
            $manager->flush();
        }
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 5;
    }

    /**
     * @return array
     */
    private function getRules()
    {
        return [
            [1, 'min', 3],
            [1, 'max', 255],
            [2, 'min', 1],
            [2, 'max', 5],
            [3, 'type', 'bool'],
        ];
    }

}
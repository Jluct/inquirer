<?php

namespace Jluct\AskBundle\Controller;


use AppBundle\Entity\PageMeta;
use Jluct\AskBundle\Form\AnswerType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Jluct\AskBundle\Entity\Answer;
use Jluct\AskBundle\Service\Tree;
use Symfony\Component\HttpFoundation\Request;
use Jluct\AskBundle\Entity\Category;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class DefaultController
 * @package Jluct\AskBundle\Controller
 */
class DefaultController extends Controller
{
    const PATH_ASK_INDEX = 'ask_homepage';

    /**
     * @param Request $request
     *
     * @var Tree $tree
     * @var array $data
     * @var Category $t
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $locale = $request->getLocale();
        $parents = null;
        $chain = [];
        $currentCategory = null;
        $chainUrl = '';
        $em = $this->getDoctrine()->getRepository('AskBundle:Category');
        $url = array_diff(explode('/', $request->get('url')), ['']);
        foreach ($url as $value) {
            if (!((int)$value)) {
                return new Response(sprintf("path /%s not found", $request->get('url')), Response::HTTP_NOT_FOUND);
            }
        }

        if (!empty($url[0])) {
            $chain = $em->getTreeLine($url, $locale);
            $tree = $this->get('ask.tree');
            if (!$tree->isChain($chain)) {
                return new Response("path /" . $request->get('url') . " not found", Response::HTTP_NOT_FOUND);
            }
            $currentCategory = $chain[count($chain) - 1];
            $chainUrl = $tree->getChainUrl($chain);
        } else {
            $parents = $em->getParentCategory($locale);
        }
        $this->buildBreadcrumbs($chain);

        return $this->render('AskBundle:Default:category.html.twig',
            [
                'parents' => $parents,
                'chainUrl' => $chainUrl,
                'currentCategory' => $currentCategory,
                '_csrf' => $this->get('security.csrf.token_manager')->getToken('question_form')

            ]
        );
    }

    /**
     * @param Request $request
     *
     * @var ValidatorInterface $validator
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function answerAction(Request $request)
    {
        $url = $request->server->get('HTTP_REFERER');
        $submittedToken = $request->get('_csrf');
        if (!$this->isCsrfTokenValid('question_form', $submittedToken)) {
            $this->addFlash(
                'error',
                'Ошибка формы. Обновите страницу'
            );

            return $this->redirect($url);
        }
        $doctrine = $this->getDoctrine();
        $data = $request->get('answer');
        $data['user'] = $this->getUser()->getId();
        $request->request->set('user', $this->getUser());
        $answer = $this->getAnswer($data);
        $this->container->get('question_type.factory')->setQuestion($answer->getQuestion());
        $form = $this->createForm(AnswerType::class, $answer);
        $form->handleRequest($request);
        $form->isSubmitted();
        if (!$form->isValid()) {
            $errors = $form->getErrors(true);
            foreach ($errors as $item) {
                $this->addFlash(
                    'danger',
                    $item->getMessage()
                );
            }

            return $this->redirect($url);
        }
        $answer->setCreatedAt(new \DateTime());
        $em = $doctrine->getManager();
        $em->persist($answer);
        $em->flush();
        $this->addFlash(
            'success',
            'Ваш ответ принят'
        );

        return $this->redirect($url, Response::HTTP_FOUND);
    }

    /**
     * @param Request $request
     * @param $category
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirectToCategoryAction(Request $request, $category)
    {
        $locale = $request->getLocale();
        $tree = $this->get('ask.tree');
        $target = $this->getDoctrine()
            ->getRepository('AskBundle:Category')
            ->categoryLoadingWithData($category, $locale);
        $chain = $tree->loadingChain($target);
        if (!$chain) {
            throw new NotFoundHttpException();
        }
        $chainUrl = $tree->getChainUrl(array_reverse($chain));
        $router = $this->get('router');

        return $this->redirect($router->generate(self::PATH_ASK_INDEX, ['url' => $chainUrl]));
    }

    /**
     * @param array $array
     */
    private function buildBreadcrumbs(array $array = [])
    {
        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $router = $this->get('router');
        $url = [];
        $breadcrumbs->addItem(PageMeta::PAGE_HOMEPAGE, $router->generate(PageMeta::PAGE_HOMEPAGE), []);
        $breadcrumbs->addItem(self::PATH_ASK_INDEX, $router->generate(self::PATH_ASK_INDEX), []);
        /** @var Category $item */
        foreach ($array as $item) {
            $url[] = $item->getId();
            $breadcrumbs->addItem(
                $item->getData()[0]->getName(),
                $router->generate(self::PATH_ASK_INDEX, ['url' => implode('/', $url)]), []
            );
        }
    }

    /**
     * @param array $data
     * @return Answer
     */
    private function getAnswer(array $data)
    {
        /** @var Answer $answer */
        $answer = $this->getDoctrine()->getRepository('AskBundle:Answer')->findExistAnswer($data);
        if (empty($answer)) {
            return $this->makeAnswer($data);
        }
        $dateSeparator = $this->container->getParameter('date_separator');
        $answerTimeStamp = $answer->getCreatedAt()->getTimestamp();
        $currentTimePoint = mktime(0, 0, 0, date("m"), $dateSeparator, date("Y"));
        $prevTimePoint = mktime(0, 0, 0, date("m") - 1, $dateSeparator, date("Y"));
        $date = (new \DateTime())->getTimestamp();

        if (
            (($answerTimeStamp > $prevTimePoint && $answerTimeStamp <= $currentTimePoint) &&
                ($date > $prevTimePoint && $date <= $currentTimePoint)) ||
            ($answerTimeStamp > $currentTimePoint && $date > $currentTimePoint)
        ) {
            return $answer;
        }

        return $this->makeAnswer($data);
    }

    /**
     * @param array $data
     * @return Answer
     */
    private function makeAnswer(array $data)
    {
        $answer = new Answer();
        $answer->setQuestion(
            $this->getDoctrine()
                ->getRepository('AskBundle:Question')
                ->findOneBy(['id' => $data['question'], 'active' => true]));
        $answer->setUser($this->getUser());

        return $answer;
    }
}

<?php

namespace Jluct\UserBundle\Entity;

use AppBundle\Entity\Feedback;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User
 *
 * @ORM\Table(name="app_user")
 * @ORM\Entity(repositoryClass="Jluct\UserBundle\Repository\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="user_name", type="string", nullable=false, length=255)
     */
    protected $name;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Jluct\AskBundle\Entity\Answer", mappedBy="user")
     */
    protected $answers;

    /**
     * @var Feedback
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Feedback", mappedBy="user")
     */
    protected $feedback;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * User constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->answers = new ArrayCollection();
        $this->feedback = new ArrayCollection();
    }

    /**
     * Add answer
     *
     * @param \Jluct\AskBundle\Entity\Answer $answer
     *
     * @return User
     */
    public function addAnswer(\Jluct\AskBundle\Entity\Answer $answer)
    {
        $this->answers[] = $answer;

        return $this;
    }

    /**
     * Remove answer
     *
     * @param \Jluct\AskBundle\Entity\Answer $answer
     */
    public function removeAnswer(\Jluct\AskBundle\Entity\Answer $answer)
    {
        $this->answers->removeElement($answer);
    }

    /**
     * Get answers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * Add feedback
     *
     * @param \AppBundle\Entity\Feedback $feedback
     *
     * @return User
     */
    public function addFeedback(\AppBundle\Entity\Feedback $feedback)
    {
        $this->feedback[] = $feedback;

        return $this;
    }

    /**
     * Remove feedback
     *
     * @param \AppBundle\Entity\Feedback $feedback
     */
    public function removeFeedback(\AppBundle\Entity\Feedback $feedback)
    {
        $this->feedback->removeElement($feedback);
    }

    /**
     * Get feedback
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFeedback()
    {
        return $this->feedback;
    }
}

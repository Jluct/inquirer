<?php

namespace AdminBundle\Admin;


use Jluct\AskBundle\Entity\Category;
use Jluct\AskBundle\Entity\TypeQuestion;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;


/**
 * Class CategoryAdmin
 * @package AdminBundle\Admin
 */
class QuestionAdmin extends AbstractAdmin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Данные вопроса', ['class' => 'col-md-8'])
            ->add('text', TextType::class, [
                'label' => 'Текст критерия'
            ])
            ->add('active', CheckboxType::class, [
                'label' => 'Активность',
                'required' => false
            ])
            ->end()
            ->with('Мета информация', ['class' => 'col-md-4'])
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'label' => 'Категория',
                'choice_label' => function ($category) {
                    foreach ($category->getData() as $item) {
                        if ($item->getLang() == 'ru') {
                            return $item->getName();
                        }
                    }

                    return $category->getData()[0]->getName();
                },
                'placeholder' => 'Выберите категорию'
            ])
            ->add('type', EntityType::class, [
                'class' => TypeQuestion::class,
                'label' => 'Тип вопроса',
                'choice_label' => 'name',
                'placeholder' => 'Выберите тип вопроса'
            ])
            ->end();
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('text', null, [
            'label' => 'Критерий'
        ]);
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('text')
            ->addIdentifier('category', null, [
                'associated_property' => function ($category) {
                    foreach ($category->getData() as $item) {
                        if ($item->getLang() == 'ru') {
                            return $item->getName();
                        }
                    }

                    return $category->getData()[0]->getName();
                },
            ])
            ->addIdentifier('active', 'boolean', [
                'label' => 'Активность'
            ]);
    }
}
<?php

namespace AdminBundle\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Class AnswerAdmin
 * @package AdminBundle\Admin
 */
class AnswerAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('id', TextType::class);

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('value', null, [
                'label' => 'Оценка'
            ]);
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('question', null, [
                    'label' => 'Критерий оценки',
                    'associated_property' => 'text',
                    'header_style' => 'width: 70%;',
                ]
            )
            ->addIdentifier('value', null, [
                'label' => 'Оценка',
                'header_style' => 'width: 10%;',
            ])
            ->addIdentifier('user', null, [
                'label' => 'Логин пользователя',
                'associated_property' => 'username',
                'header_style' => 'width: 20%;',
            ]);
    }

    /**
     * @return array
     */
    public function getExportFields()
    {
        return ['Критерий оценки' => 'question.text', 'Оценка' => 'value', 'Логин пользователя' => 'user.username'];
    }
}
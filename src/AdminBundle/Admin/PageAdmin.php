<?php

namespace AdminBundle\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ChoiceFieldMaskType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Class AnswerAdmin
 * @package AdminBundle\Admin
 */
class PageAdmin extends AbstractAdmin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('lang', ChoiceFieldMaskType::class, [
                'label' => 'Язык страницы',
                'placeholder' => 'Выберите язык',
                'choices' => [
                    'Русский' => 'en',
                    'Английский' => 'ru',
                ],
            ])
            ->add('title', TextType::class, [
                'label' => 'Заголовок'
            ])
            ->add('content', TextareaType::class, [
                'label' => 'Контент',
                'attr' => ['rows' => 12]
            ])
            ->add('meta', EntityType::class, [
                'label'=>'Системная информация',
                'class' => 'AppBundle\Entity\PageMeta',
                'choice_label' => 'label'
            ]);

    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title', null, [
                    'label' => 'Заголовок',
                ]
            )
            ->add('lang', null, [
                    'label' => 'Язык',
                ]
            );
    }
}
<?php

namespace AdminBundle\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Form\Type\BooleanType;

/**
 * Class AnswerAdmin
 * @package AdminBundle\Admin
 */
class UserAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('enabled', BooleanType::class, [
                'label' => 'Активность'
            ]);

    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('username', null, [
                    'label' => 'Логин',
                ]
            )
            ->add('email', null, [
                    'label' => 'Эл. почта',
                ]
            )
            ->add('enabled', null, [
                    'label' => 'Активность',
                ]
            );
    }
}
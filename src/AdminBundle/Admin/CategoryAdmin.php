<?php

namespace AdminBundle\Admin;


use Jluct\AskBundle\Entity\Category;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

/**
 * Class CategoryAdmin
 * @package AdminBundle\Admin
 */
class CategoryAdmin extends AbstractAdmin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Категория', ['class' => 'col-md-9'])
            ->add('active', CheckboxType::class, [
                'label' => 'Активность'
            ])
//            ->add('data.name', null, [])
            ->end()
            ->with('Мета информация', ['class' => 'col-md-3'])
            ->add('parent', EntityType::class, [
                'class' => Category::class,
                'required' => false,
                'choice_label' => function ($category) {
                    foreach ($category->getData() as $item) {
                        if ($item->getLang() == 'ru') {
                            return $item->getName();
                        }
                    }

                    return $category->getData()[0]->getName();
                },
                'placeholder' => 'Выберите родительскую категорию'
            ])
            ->end();
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('data', null, [
                'label' => 'Категория',
                'associated_property' => 'name'
            ])
            ->addIdentifier('active', 'boolean', [
                'label' => 'Активность',
            ]);
    }
}
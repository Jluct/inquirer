/**
 yarn run encore dev
 yarn run encore dev --watch
 yarn run encore production
 */

window.$ = require('jquery');

require('button-visually-impaired-jquery/dist/css/bvi.min.css');
require('button-visually-impaired-jquery/dist/css/bvi-font.min.css');
window.responsiveVoice = require('button-visually-impaired-jquery/dist/js/responsivevoice');
require('button-visually-impaired-jquery/dist/js/bvi.min');
window.Cookies = require('button-visually-impaired-jquery/dist/js/js.cookie.min');

require('chart.js');
require('bootstrap-sass');
require('../css/app.scss');

$('[data-toggle="popover"]').popover();

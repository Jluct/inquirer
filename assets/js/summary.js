(function() {
    window.SummaryChart = function (conf) {
        var _self = this;
        _self.labels = conf.labels;
        _self.data = conf.data;
        _self.canvas = conf.canvas;

        _self.barChart = new Chart(_self.canvas, {
            type: 'bar',
            data: {
                labels: _self.labels,
                datasets: [{
                    label: 'Среднее значение',
                    data: _self.data,
                    backgroundColor: 'rgba(1,0,255,0.5)',
                    borderColor: 'rgba(1,0,255,0.7)',
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            max: 5
                        }
                    }]
                }
            }
        });
    };
})();